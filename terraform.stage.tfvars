#Server name

server_name   = "postgre-stage-cluster"
instance_name = ["postgre-stage"]
user_database = "master"

#RDS Instance
replica_count                = 1
db_subnet_group_name         = "subnet-dbs-stage"
vpc_security_group_ids       = ["sg-2f370047"]
backup_retention_period      = 30
preferred_backup_window      = "01:00-02:00"
preferred_maintenance_window = "mon:02:00-mon:04:00"
instance_class               = "db.r4.large"
monitoring_interval          = 0

#aws_route53_record
dns_name = ["postgre-stage.database.beblue.com.br"]

#aws_security_group
prod                  = false
security_group_name   = "staging-3a50077c-rds-postgresql"
security_tag_name     = "staging-3a50077c-rds-postgresql"
security_group_desc   = "PostgreSQL stage instances"
security_group_vpc_id = "vpc-a586b1cc"
#aws_security_group_rule
security_group_id       = "sg-2f370047"
security_group_sql_port = 5432
security_group_sql_prod = false