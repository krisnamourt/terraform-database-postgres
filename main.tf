resource "aws_rds_cluster_instance" "postgres-cluster-instances" {
  count               = var.replica_count
  identifier          = var.instance_name[count.index]
  cluster_identifier  = "${aws_rds_cluster.postgres-cluster.id}"
  engine              = "aurora-postgresql"
  engine_version      = "9.6.12"
  instance_class      = "${var.instance_class}"
  promotion_tier      = 1
  monitoring_interval = var.monitoring_interval
}

resource "aws_rds_cluster" "postgres-cluster" {
  cluster_identifier                  = "${var.server_name}"
  availability_zones                  = ["us-east-2a", "us-east-2b", "us-east-2c"]
  master_username                     = "${var.user_database}"
  master_password                     = "${var.user_password}"
  backtrack_window                    = 0
  backup_retention_period             = var.backup_retention_period
  db_cluster_parameter_group_name     = "${aws_db_parameter_group.rds-postgres-param.name}"
  db_subnet_group_name                = "${var.db_subnet_group_name}"
  deletion_protection                 = true
  engine                              = "aurora-postgresql"
  engine_mode                         = "provisioned"
  engine_version                      = "9.6.12"
  iam_database_authentication_enabled = false
  kms_key_id                          = "arn:aws:kms:us-east-2:743204975336:key/a993a80f-f4d8-47c3-b84e-aa0c480b1948"
  port                                = 5432
  preferred_backup_window             = "${var.preferred_backup_window}"
  preferred_maintenance_window        = "${var.preferred_maintenance_window}"
  skip_final_snapshot                 = true
  storage_encrypted                   = true
  vpc_security_group_ids              = var.vpc_security_group_ids
  cluster_members                     = var.instance_name
  tags                                = {}
}

resource "aws_route53_record" "database_dns" {

  name = "${var.dns_name[0]}"

  records = (var.prod == true
    ? ["${aws_rds_cluster.postgres-cluster.endpoint}"]
    : ["${aws_rds_cluster_instance.postgres-cluster-instances[0].endpoint}"]
  )

  ttl     = 60
  type    = "CNAME"
  zone_id = "ZIKYJRTS2RU31"
}

resource "aws_db_parameter_group" "rds-postgres-param" {
  name        = "prod-aurora-postgresql96"
  description = "prod-aurora-postgresql96"
  family      = "aurora-postgresql9.6"

  parameter {
    apply_method = "immediate"
    name         = "backend_flush_after"
    value        = "0"
  }
  parameter {
    apply_method = "immediate"
    name         = "bgwriter_flush_after"
    value        = "32"
  }
  parameter {
    apply_method = "immediate"
    name         = "checkpoint_flush_after"
    value        = "16"
  }
  parameter {
    apply_method = "immediate"
    name         = "default_statistics_target"
    value        = "100"
  }
  parameter {
    apply_method = "immediate"
    name         = "effective_cache_size"
    value        = "1440000"
  }
  parameter {
    apply_method = "immediate"
    name         = "log_min_duration_statement"
    value        = "20000"
  }
  parameter {
    apply_method = "immediate"
    name         = "log_statement"
    value        = "none"
  }
  parameter {
    apply_method = "immediate"
    name         = "maintenance_work_mem"
    value        = "960000"
  }
  parameter {
    apply_method = "immediate"
    name         = "max_parallel_workers_per_gather"
    value        = "1"
  }
  parameter {
    apply_method = "immediate"
    name         = "pg_stat_statements.save"
    value        = "1"
  }
  parameter {
    apply_method = "immediate"
    name         = "random_page_cost"
    value        = "2"
  }
  parameter {
    apply_method = "immediate"
    name         = "work_mem"
    value        = "7864"
  }
  parameter {
    apply_method = "pending-reboot"
    name         = "max_wal_senders"
    value        = "5"
  }
  parameter {
    apply_method = "pending-reboot"
    name         = "max_worker_processes"
    value        = "2"
  }
  parameter {
    apply_method = "pending-reboot"
    name         = "shared_buffers"
    value        = "600000"
  }
  parameter {
    apply_method = "pending-reboot"
    name         = "shared_preload_libraries"
    value        = "pg_stat_statements"
  }
  parameter {
    apply_method = "pending-reboot"
    name         = "track_activity_query_size"
    value        = "16384"
  }

  # parameter {
  #   name         = "autovacuum_vacuum_scale_factor"
  #   value        = "0.05"
  #   apply_method = "pending-reboot"
  # }

  # parameter {
  #   name         = "autovacuum_max_workers"
  #   value        = "5"
  #   apply_method = "pending-reboot"
  # }

}

# Only at PROD
resource "aws_route53_record" "database_dns_replica" {
  count = (var.prod == true ? 1 : 0)

  name = var.dns_name[1]
  records = [
    "${aws_rds_cluster.postgres-cluster.reader_endpoint}",
  ]
  ttl     = 60
  type    = "CNAME"
  zone_id = "ZIKYJRTS2RU31"
}


resource "aws_sns_topic" "postgres-sns-topic" {
  count = (var.prod == true ? 1 : 0)
  name  = "postgres-sns-topic-prod"
}


resource "aws_cloudwatch_metric_alarm" "postgres_cpu_utilization" {
  count               = (var.prod == true ? var.replica_count : 0)
  alarm_name          = "postgres-cpu-alarm-prod-${count.index}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/RDS"
  period              = "300"
  statistic           = "Average"
  threshold           = "80"
  alarm_description   = "This metric monitors postgres rds cpu utilization"
  alarm_actions       = ["${aws_sns_topic.postgres-sns-topic[0].arn}"]
  dimensions = {
    DBInstanceIdentifier = "${aws_rds_cluster_instance.postgres-cluster-instances[count.index].identifier}"
  }

}



resource "aws_security_group" "vpc-security-group" {
  name        = "${var.security_group_name}"
  description = "${var.security_group_desc}"
  vpc_id      = "${var.security_group_vpc_id}"

  tags = {
    Name = "${var.security_tag_name}"
  }
}


resource "aws_security_group_rule" "vpc-security-group" {

  cidr_blocks       = (var.security_group_sql_prod == true ? ["0.0.0.0/0", ] : ["0.0.0.0/0", ])
  description       = (var.security_group_sql_prod == true ? "" : "Allow access to local network")
  from_port         = (var.security_group_sql_prod == true ? 0 : var.security_group_sql_port)
  ipv6_cidr_blocks  = []
  prefix_list_ids   = []
  protocol          = (var.security_group_sql_prod == true ? "-1" : "tcp")
  security_group_id = "${var.security_group_id}"
  self              = false
  to_port           = (var.security_group_sql_prod == true ? 0 : var.security_group_sql_port)
  type              = (var.security_group_sql_prod == true ? "egress" : "ingress")
}


resource "aws_security_group_rule" "vpc-security-group-1" {
  cidr_blocks       = (var.security_group_sql_prod == true ? ["172.31.0.0/16"] : ["0.0.0.0/0"])
  description       = (var.security_group_sql_prod == true ? "Allow postgres do local network" : "")
  from_port         = (var.security_group_sql_prod == true ? var.security_group_sql_port : 0)
  ipv6_cidr_blocks  = []
  prefix_list_ids   = []
  protocol          = (var.security_group_sql_prod == true ? "tcp" : "-1")
  security_group_id = "${var.security_group_id}"
  self              = false
  to_port           = (var.security_group_sql_prod == true ? var.security_group_sql_port : 0)
  type              = (var.security_group_sql_prod == true ? "ingress" : "egress")
}

# Only at PROD
resource "aws_security_group_rule" "vpc-security-group-2" {
  count             = (var.prod == true ? 1 : 0)
  cidr_blocks       = []
  from_port         = 5432
  ipv6_cidr_blocks  = []
  prefix_list_ids   = []
  protocol          = "tcp"
  security_group_id = "${var.security_group_id}"
  self              = false
  to_port           = 5432
  type              = "ingress"
}