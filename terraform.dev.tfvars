#Server name

server_name   = "postgre-dev-cluster"
instance_name = ["postgre-dev"]
user_database = "master"

#RDS Instance
replica_count                = 1
db_subnet_group_name         = "subnet-dbs-dev"
vpc_security_group_ids       = ["sg-2d3a6845"]
backup_retention_period      = 35
preferred_backup_window      = "01:00-02:00"
preferred_maintenance_window = "mon:02:00-mon:04:00"
instance_class               = "db.r4.large"
monitoring_interval          = 0

#aws_route53_record
dns_name = ["postgre-dev.database.beblue.com.br"]

#aws_security_group
prod                  = false
security_group_name   = "dev-06bdcc01-rds-postgresql"
security_tag_name     = "dev-06bdcc01-rds-postgresql"
security_group_desc   = "dev-06bdcc01-rds-postgresql"
security_group_vpc_id = "vpc-51f7e838"
#aws_security_group_rule
security_group_id       = "sg-2d3a6845"
security_group_sql_port = 5432
security_group_sql_prod = false