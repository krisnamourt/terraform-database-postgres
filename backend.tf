terraform {
  backend "s3" {
    bucket = "terraform-state-database-postgres.beblue.com.br"
    key    = "terraform/terraform.tfstate"
    region = "us-east-1"
  }
}
