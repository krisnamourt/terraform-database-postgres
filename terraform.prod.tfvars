#Server name

server_name   = "postgre-prod-cluster"
instance_name = ["postgre-prod-us-east-2c", "postgre-prod"]
user_database = "beblueadmin"

#RDS Instance
replica_count                = 2
db_subnet_group_name         = "subnet-dbs-prod"
vpc_security_group_ids       = ["sg-839727eb"]
backup_retention_period      = 35
preferred_backup_window      = "03:00-04:00"
preferred_maintenance_window = "mon:01:00-mon:03:00"
instance_class               = "db.r4.large"
monitoring_interval          = 60

#aws_route53_record
dns_name = ["postgre-prod.database.beblue.com.br", "postgre-prod-replica.database.beblue.com.br"]

#aws_security_group
prod                  = true
security_group_name   = "allow-postgres-localnetwork"
security_tag_name     = "prod-75b86059-rds-postgresql"
security_group_desc   = "Allow postgres port on local network"
security_group_vpc_id = "vpc-b67852df"
#aws_security_group_rule
security_group_id       = "sg-839727eb"
security_group_sql_port = 5432
security_group_sql_prod = true