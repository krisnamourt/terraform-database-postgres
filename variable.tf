variable "aws_region" {
  description = "The AWS region to deploy into (e.g. us-east-2)."
  default     = "us-east-2"
}


variable "replica_count" {
  description = "Number of cluster members."
}


variable "db_subnet_group_name" {
  description = "Subnet group name."
}

variable "vpc_security_group_ids" {
  description = "The VPC secutiry group id."
  type        = list(string)
}

variable "preferred_backup_window" {
  description = "Preferred backup window"
}

variable "preferred_maintenance_window" {
  description = "Preferred maintenance window"
}

variable "backup_retention_period" {
  description = "Backup retention period."
}


variable "instance_class" {
  description = "Resource instance class."
}

variable "monitoring_interval" {
  description = "Monitoring interval."
}

variable "server_name" {
  description = "The server name at RDS."
}

variable "instance_name" {
  description = "Instance resources."
  type        = list(string)
}


variable "user_database" {
  description = "The database user name."
}

variable "user_password" {
  description = "The database user password."
}

variable "prod" {
  description = "one more security group rules and two DNS at Route 53"

}

#aws_route53_record

variable "dns_name" {

}

#aws_security_group

variable "security_group_name" {

}

variable "security_group_desc" {

}

variable "security_group_vpc_id" {

}

variable "security_tag_name" {

}

#aws_security_group_rule

variable "security_group_id" {

}

variable "security_group_sql_port" {
  type = number
}

variable "security_group_sql_prod" {
  description = "Indicates a different setup of rule."
}